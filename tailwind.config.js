module.exports = {
 purge: [
    './src/**/*.html',
    './src/**/*.vue',
    './src/**/*.jsx',
  ],
  theme: {
    extend: {
	    opacity: {
         '90': '.9',
	    }, 
	    spacing: {
         '100': '40rem',
         '110': '50rem',
	    }, 
	    height: {
       hero: 'calc(100vh - 4.5rem)',
       heromobile: 'calc(100vh - 4rem)',
	    }, 
	    fontFamily: {
              'DEFAULT': ['Open Sans', 'sans-serif'], 
            },  
	    colors: {
	      primary: {
		    DEFAULT: '#fbd553',
	      },
	      back: {
		    DEFAULT: '#042F4E',
		    100: '#bad4e7',
		    200: '#0573C7',
		    300: '#0567B3',
		    400: '#045C9F',
		    500: '#04508B',
		    600: '#044577',
		    700: '#043962',
		    800: '#02223B',
		    900: '#011727',
	      },
	      danger: {
		    DEFAULT: '#EF5350',
		    100: '#FDEDEC',
		    200: '#FAC87C',
		    300: '#F5918F',
		    400: '#F16C6A',
		    500: '#EC3532',
		    600: '#CD1613',
		    700: '#A81210',
		    800: '#95100E',
		    900: '#700C0A',
	      },
	      success: {
		    DEFAULT: '#22DA6E',
		    100: '#DCF9E8',
		    200: '#B8F4D1',
		    300: '#95EEBA',
		    400: '#72E9A3',
		    500: '#4FE38D',
		    600: '#1EC263',
		    700: '#199F51',
		    800: '#137C3F',
		    900: '#0E582D',
	      },
	      info: {
		    DEFAULT: '#A656DC',
		    100: '#F6EEFC',
		    200: '#EDDDF8',
		    300: '#DCBBF1',
		    400: '#B878E3',
		    500: '#A656DC',
		    600: '#9534D5',
		    700: '#7F26BA',
		    800: '#681F98',
		    900: '#451566',
	      },
	    },  
    },
  },
  variants: {},
  plugins: [
    require('@tailwindcss/forms'),
    require('@tailwindcss/typography'),
    require('@tailwindcss/aspect-ratio'),
  ],
}
