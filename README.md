# Refinable.io Frontend

*The frontend for [refinable.io](https://refinable.io) - built with vuejs.*

**This microservice is part of the refinable.io stack:**

- [rio-frontend](https://gitlab.com/msvechla/rio-frontend) - the vuejs frontend
- [rio-gateway](https://gitlab.com/msvechla/rio-gateway) - the main user-facing gateway
- [rio-session](https://gitlab.com/msvechla/rio-sessions) - microservice for issuing session tokes and creating games
- [rio-gameserver](https://gitlab.com/msvechla/rio-gameserver) - service hosting the main refining session logic

An overview of the microservice architecture can be found [here](https://gitlab.com/msvechla/rio-gameserver/-/blob/master/docs/architecture.md).

## Table of Contents

<!-- TOC -->

- [Capabilities](#capabilities)
- [Local Development](#local-development)
- [Deployment](#deployment)
- [Feature Requests and Bug Reports](#feature-requests-and-bug-reports)
- [Contributing](#contributing)
- [Versioning](#versioning)
- [Authors](#authors)
- [License](#license)

<!-- /TOC -->

## Capabilities

- automatic reconnect on websocket connection drop
- securely exchanging authenticated messages via wss with the gateway service
- works great on desktop and mobile
- joining sessions via QR code

> DISCLAIMER: This frontend is still WIP and one of my first VueJS projects. The code is far from perfect and merely a starting point for future work.

## Local Development

```sh
yarn serve
```

## Deployment

The vuejs frontend is deployed as static content behind a CDN.

## Feature Requests and Bug Reports

We welcome feature requests and bug reports for refinable.io. Please open an issue [here](https://gitlab.com/msvechla/rio-frontend/-/issues) so we can prioritize and track the progress.

## Contributing

Please read [CONTRIBUTING.md]() for details on our code of conduct, and the process for submitting pull requests to us.

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://gitlab.com/msvechla/rio-frontend/tags) or take a look at the [CHANGELOG.md](./CHANGELOG.md)

## Authors

- **Marius Svechla** - *Initial work*

See also the list of [contributors](https://gitlab.com/msvechla/rio-frontend/graphs/master) who participated in this project.

## License

[MIT License](./License.md)

Copyright (c) [2020] [Marius Svechla]
