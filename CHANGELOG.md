# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [2.5.1] - 2021-05-12

### Fixed

- use correct error code to redirect users, prevent redirecting users back to login on gateway timeouts

## [2.5.0] - 2021-04-18

### Added

- automatically redirect users to the correct join URL when falsely trying to join via host url (fixes [#2](https://gitlab.com/msvechla/rio-frontend/-/issues/2)) 

## [2.4.0] - 2021-04-14

### Added

- error notifications for the landing page

## [2.3.0] - 2020-11-26

### Added

- moved to tailwindcss v2

## [2.2.0] - 2020-11-10

### Added

- option to specify link to game topics

## [2.1.0] - 2020-11-05

### Added

- QR-Code for inviting team-members

## [2.0.0] - 2020-10-27

### Added

- redesign with tailwindcss
- switched to typescript
- switched from json to protobuf api

## [1.0.0] - 2020-04-09

### Added

- initial version
