proto: pbts pbjs

pbjs:
	pbjs -t static-module -o src/rio-proto.js --es6 -w es6 ../rio-gameserver/pkg/communication/messaging/types.proto

pbts: pbjs
	pbts -o src/rio-proto.d.ts src/rio-proto.js

