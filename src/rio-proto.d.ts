import * as $protobuf from "protobufjs";
/** Namespace messaging. */
export namespace messaging {

    /** Properties of a Connection. */
    interface IConnection {

        /** Connection hosting */
        hosting?: (boolean|null);

        /** Connection state */
        state?: (messaging.Connection.State|null);
    }

    /** Represents a Connection. */
    class Connection implements IConnection {

        /**
         * Constructs a new Connection.
         * @param [properties] Properties to set
         */
        constructor(properties?: messaging.IConnection);

        /** Connection hosting. */
        public hosting: boolean;

        /** Connection state. */
        public state: messaging.Connection.State;

        /**
         * Creates a new Connection instance using the specified properties.
         * @param [properties] Properties to set
         * @returns Connection instance
         */
        public static create(properties?: messaging.IConnection): messaging.Connection;

        /**
         * Encodes the specified Connection message. Does not implicitly {@link messaging.Connection.verify|verify} messages.
         * @param message Connection message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: messaging.IConnection, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Encodes the specified Connection message, length delimited. Does not implicitly {@link messaging.Connection.verify|verify} messages.
         * @param message Connection message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: messaging.IConnection, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Decodes a Connection message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns Connection
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): messaging.Connection;

        /**
         * Decodes a Connection message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns Connection
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): messaging.Connection;

        /**
         * Verifies a Connection message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);

        /**
         * Creates a Connection message from a plain object. Also converts values to their respective internal types.
         * @param object Plain object
         * @returns Connection
         */
        public static fromObject(object: { [k: string]: any }): messaging.Connection;

        /**
         * Creates a plain object from a Connection message. Also converts values to other types if specified.
         * @param message Connection
         * @param [options] Conversion options
         * @returns Plain object
         */
        public static toObject(message: messaging.Connection, options?: $protobuf.IConversionOptions): { [k: string]: any };

        /**
         * Converts this Connection to JSON.
         * @returns JSON object
         */
        public toJSON(): { [k: string]: any };
    }

    namespace Connection {

        /** State enum. */
        enum State {
            CONNECTION = 0,
            OPEN = 1,
            INIT = 2,
            ESTABLISHED = 3,
            DISCONNECTED = 4
        }
    }

    /** SpecialUserID enum. */
    enum SpecialUserID {
        HOST = 0
    }

    /** GameStatus enum. */
    enum GameStatus {
        NEW = 0,
        RUNNING = 1,
        FINISHED = 2
    }

    /** Properties of a GameStatusUpdate. */
    interface IGameStatusUpdate {

        /** GameStatusUpdate playerStatus */
        playerStatus?: ({ [k: string]: messaging.CardDeckCards }|null);

        /** GameStatusUpdate highlightedPlayers */
        highlightedPlayers?: (string[]|null);

        /** GameStatusUpdate majorityJoinedPlayers */
        majorityJoinedPlayers?: (string[]|null);

        /** GameStatusUpdate majorityVotedCard */
        majorityVotedCard?: (messaging.CardDeckCards|null);

        /** GameStatusUpdate unanimousVote */
        unanimousVote?: (boolean|null);

        /** GameStatusUpdate hosts */
        hosts?: (string[]|null);

        /** GameStatusUpdate gameStatus */
        gameStatus?: (messaging.GameStatus|null);

        /** GameStatusUpdate gameTitle */
        gameTitle?: (string|null);

        /** GameStatusUpdate gameDeck */
        gameDeck?: (string|null);

        /** GameStatusUpdate gameTopic */
        gameTopic?: (string|null);

        /** GameStatusUpdate gameRound */
        gameRound?: (number|null);

        /** GameStatusUpdate cardDeck */
        cardDeck?: (messaging.CardDeckType|null);

        /** GameStatusUpdate gameTopicLink */
        gameTopicLink?: (string|null);
    }

    /** Represents a GameStatusUpdate. */
    class GameStatusUpdate implements IGameStatusUpdate {

        /**
         * Constructs a new GameStatusUpdate.
         * @param [properties] Properties to set
         */
        constructor(properties?: messaging.IGameStatusUpdate);

        /** GameStatusUpdate playerStatus. */
        public playerStatus: { [k: string]: messaging.CardDeckCards };

        /** GameStatusUpdate highlightedPlayers. */
        public highlightedPlayers: string[];

        /** GameStatusUpdate majorityJoinedPlayers. */
        public majorityJoinedPlayers: string[];

        /** GameStatusUpdate majorityVotedCard. */
        public majorityVotedCard: messaging.CardDeckCards;

        /** GameStatusUpdate unanimousVote. */
        public unanimousVote: boolean;

        /** GameStatusUpdate hosts. */
        public hosts: string[];

        /** GameStatusUpdate gameStatus. */
        public gameStatus: messaging.GameStatus;

        /** GameStatusUpdate gameTitle. */
        public gameTitle: string;

        /** GameStatusUpdate gameDeck. */
        public gameDeck: string;

        /** GameStatusUpdate gameTopic. */
        public gameTopic: string;

        /** GameStatusUpdate gameRound. */
        public gameRound: number;

        /** GameStatusUpdate cardDeck. */
        public cardDeck: messaging.CardDeckType;

        /** GameStatusUpdate gameTopicLink. */
        public gameTopicLink: string;

        /**
         * Creates a new GameStatusUpdate instance using the specified properties.
         * @param [properties] Properties to set
         * @returns GameStatusUpdate instance
         */
        public static create(properties?: messaging.IGameStatusUpdate): messaging.GameStatusUpdate;

        /**
         * Encodes the specified GameStatusUpdate message. Does not implicitly {@link messaging.GameStatusUpdate.verify|verify} messages.
         * @param message GameStatusUpdate message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: messaging.IGameStatusUpdate, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Encodes the specified GameStatusUpdate message, length delimited. Does not implicitly {@link messaging.GameStatusUpdate.verify|verify} messages.
         * @param message GameStatusUpdate message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: messaging.IGameStatusUpdate, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Decodes a GameStatusUpdate message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns GameStatusUpdate
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): messaging.GameStatusUpdate;

        /**
         * Decodes a GameStatusUpdate message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns GameStatusUpdate
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): messaging.GameStatusUpdate;

        /**
         * Verifies a GameStatusUpdate message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);

        /**
         * Creates a GameStatusUpdate message from a plain object. Also converts values to their respective internal types.
         * @param object Plain object
         * @returns GameStatusUpdate
         */
        public static fromObject(object: { [k: string]: any }): messaging.GameStatusUpdate;

        /**
         * Creates a plain object from a GameStatusUpdate message. Also converts values to other types if specified.
         * @param message GameStatusUpdate
         * @param [options] Conversion options
         * @returns Plain object
         */
        public static toObject(message: messaging.GameStatusUpdate, options?: $protobuf.IConversionOptions): { [k: string]: any };

        /**
         * Converts this GameStatusUpdate to JSON.
         * @returns JSON object
         */
        public toJSON(): { [k: string]: any };
    }

    /** Properties of a PlayerUpdate. */
    interface IPlayerUpdate {

        /** PlayerUpdate card */
        card?: (messaging.CardDeckCards|null);
    }

    /** Represents a PlayerUpdate. */
    class PlayerUpdate implements IPlayerUpdate {

        /**
         * Constructs a new PlayerUpdate.
         * @param [properties] Properties to set
         */
        constructor(properties?: messaging.IPlayerUpdate);

        /** PlayerUpdate card. */
        public card: messaging.CardDeckCards;

        /**
         * Creates a new PlayerUpdate instance using the specified properties.
         * @param [properties] Properties to set
         * @returns PlayerUpdate instance
         */
        public static create(properties?: messaging.IPlayerUpdate): messaging.PlayerUpdate;

        /**
         * Encodes the specified PlayerUpdate message. Does not implicitly {@link messaging.PlayerUpdate.verify|verify} messages.
         * @param message PlayerUpdate message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: messaging.IPlayerUpdate, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Encodes the specified PlayerUpdate message, length delimited. Does not implicitly {@link messaging.PlayerUpdate.verify|verify} messages.
         * @param message PlayerUpdate message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: messaging.IPlayerUpdate, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Decodes a PlayerUpdate message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns PlayerUpdate
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): messaging.PlayerUpdate;

        /**
         * Decodes a PlayerUpdate message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns PlayerUpdate
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): messaging.PlayerUpdate;

        /**
         * Verifies a PlayerUpdate message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);

        /**
         * Creates a PlayerUpdate message from a plain object. Also converts values to their respective internal types.
         * @param object Plain object
         * @returns PlayerUpdate
         */
        public static fromObject(object: { [k: string]: any }): messaging.PlayerUpdate;

        /**
         * Creates a plain object from a PlayerUpdate message. Also converts values to other types if specified.
         * @param message PlayerUpdate
         * @param [options] Conversion options
         * @returns Plain object
         */
        public static toObject(message: messaging.PlayerUpdate, options?: $protobuf.IConversionOptions): { [k: string]: any };

        /**
         * Converts this PlayerUpdate to JSON.
         * @returns JSON object
         */
        public toJSON(): { [k: string]: any };
    }

    /** Properties of a HostUpdate. */
    interface IHostUpdate {

        /** HostUpdate gameStatus */
        gameStatus?: (messaging.GameStatus|null);

        /** HostUpdate gameTitle */
        gameTitle?: (string|null);

        /** HostUpdate gameTopic */
        gameTopic?: (string|null);

        /** HostUpdate gameTopicLink */
        gameTopicLink?: (string|null);
    }

    /** Represents a HostUpdate. */
    class HostUpdate implements IHostUpdate {

        /**
         * Constructs a new HostUpdate.
         * @param [properties] Properties to set
         */
        constructor(properties?: messaging.IHostUpdate);

        /** HostUpdate gameStatus. */
        public gameStatus: messaging.GameStatus;

        /** HostUpdate gameTitle. */
        public gameTitle: string;

        /** HostUpdate gameTopic. */
        public gameTopic: string;

        /** HostUpdate gameTopicLink. */
        public gameTopicLink: string;

        /**
         * Creates a new HostUpdate instance using the specified properties.
         * @param [properties] Properties to set
         * @returns HostUpdate instance
         */
        public static create(properties?: messaging.IHostUpdate): messaging.HostUpdate;

        /**
         * Encodes the specified HostUpdate message. Does not implicitly {@link messaging.HostUpdate.verify|verify} messages.
         * @param message HostUpdate message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: messaging.IHostUpdate, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Encodes the specified HostUpdate message, length delimited. Does not implicitly {@link messaging.HostUpdate.verify|verify} messages.
         * @param message HostUpdate message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: messaging.IHostUpdate, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Decodes a HostUpdate message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns HostUpdate
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): messaging.HostUpdate;

        /**
         * Decodes a HostUpdate message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns HostUpdate
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): messaging.HostUpdate;

        /**
         * Verifies a HostUpdate message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);

        /**
         * Creates a HostUpdate message from a plain object. Also converts values to their respective internal types.
         * @param object Plain object
         * @returns HostUpdate
         */
        public static fromObject(object: { [k: string]: any }): messaging.HostUpdate;

        /**
         * Creates a plain object from a HostUpdate message. Also converts values to other types if specified.
         * @param message HostUpdate
         * @param [options] Conversion options
         * @returns Plain object
         */
        public static toObject(message: messaging.HostUpdate, options?: $protobuf.IConversionOptions): { [k: string]: any };

        /**
         * Converts this HostUpdate to JSON.
         * @returns JSON object
         */
        public toJSON(): { [k: string]: any };
    }

    /** Properties of a KickUser. */
    interface IKickUser {

        /** KickUser UserID */
        UserID?: (string|null);
    }

    /** Represents a KickUser. */
    class KickUser implements IKickUser {

        /**
         * Constructs a new KickUser.
         * @param [properties] Properties to set
         */
        constructor(properties?: messaging.IKickUser);

        /** KickUser UserID. */
        public UserID: string;

        /**
         * Creates a new KickUser instance using the specified properties.
         * @param [properties] Properties to set
         * @returns KickUser instance
         */
        public static create(properties?: messaging.IKickUser): messaging.KickUser;

        /**
         * Encodes the specified KickUser message. Does not implicitly {@link messaging.KickUser.verify|verify} messages.
         * @param message KickUser message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: messaging.IKickUser, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Encodes the specified KickUser message, length delimited. Does not implicitly {@link messaging.KickUser.verify|verify} messages.
         * @param message KickUser message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: messaging.IKickUser, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Decodes a KickUser message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns KickUser
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): messaging.KickUser;

        /**
         * Decodes a KickUser message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns KickUser
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): messaging.KickUser;

        /**
         * Verifies a KickUser message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);

        /**
         * Creates a KickUser message from a plain object. Also converts values to their respective internal types.
         * @param object Plain object
         * @returns KickUser
         */
        public static fromObject(object: { [k: string]: any }): messaging.KickUser;

        /**
         * Creates a plain object from a KickUser message. Also converts values to other types if specified.
         * @param message KickUser
         * @param [options] Conversion options
         * @returns Plain object
         */
        public static toObject(message: messaging.KickUser, options?: $protobuf.IConversionOptions): { [k: string]: any };

        /**
         * Converts this KickUser to JSON.
         * @returns JSON object
         */
        public toJSON(): { [k: string]: any };
    }

    /** Properties of an OpenGame. */
    interface IOpenGame {

        /** OpenGame gameTitle */
        gameTitle?: (string|null);

        /** OpenGame cardDeck */
        cardDeck?: (messaging.CardDeckType|null);
    }

    /** Represents an OpenGame. */
    class OpenGame implements IOpenGame {

        /**
         * Constructs a new OpenGame.
         * @param [properties] Properties to set
         */
        constructor(properties?: messaging.IOpenGame);

        /** OpenGame gameTitle. */
        public gameTitle: string;

        /** OpenGame cardDeck. */
        public cardDeck: messaging.CardDeckType;

        /**
         * Creates a new OpenGame instance using the specified properties.
         * @param [properties] Properties to set
         * @returns OpenGame instance
         */
        public static create(properties?: messaging.IOpenGame): messaging.OpenGame;

        /**
         * Encodes the specified OpenGame message. Does not implicitly {@link messaging.OpenGame.verify|verify} messages.
         * @param message OpenGame message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: messaging.IOpenGame, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Encodes the specified OpenGame message, length delimited. Does not implicitly {@link messaging.OpenGame.verify|verify} messages.
         * @param message OpenGame message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: messaging.IOpenGame, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Decodes an OpenGame message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns OpenGame
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): messaging.OpenGame;

        /**
         * Decodes an OpenGame message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns OpenGame
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): messaging.OpenGame;

        /**
         * Verifies an OpenGame message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);

        /**
         * Creates an OpenGame message from a plain object. Also converts values to their respective internal types.
         * @param object Plain object
         * @returns OpenGame
         */
        public static fromObject(object: { [k: string]: any }): messaging.OpenGame;

        /**
         * Creates a plain object from an OpenGame message. Also converts values to other types if specified.
         * @param message OpenGame
         * @param [options] Conversion options
         * @returns Plain object
         */
        public static toObject(message: messaging.OpenGame, options?: $protobuf.IConversionOptions): { [k: string]: any };

        /**
         * Converts this OpenGame to JSON.
         * @returns JSON object
         */
        public toJSON(): { [k: string]: any };
    }

    /** Properties of a UserToken. */
    interface IUserToken {

        /** UserToken userID */
        userID?: (string|null);

        /** UserToken token */
        token?: (string|null);
    }

    /** Represents a UserToken. */
    class UserToken implements IUserToken {

        /**
         * Constructs a new UserToken.
         * @param [properties] Properties to set
         */
        constructor(properties?: messaging.IUserToken);

        /** UserToken userID. */
        public userID: string;

        /** UserToken token. */
        public token: string;

        /**
         * Creates a new UserToken instance using the specified properties.
         * @param [properties] Properties to set
         * @returns UserToken instance
         */
        public static create(properties?: messaging.IUserToken): messaging.UserToken;

        /**
         * Encodes the specified UserToken message. Does not implicitly {@link messaging.UserToken.verify|verify} messages.
         * @param message UserToken message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: messaging.IUserToken, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Encodes the specified UserToken message, length delimited. Does not implicitly {@link messaging.UserToken.verify|verify} messages.
         * @param message UserToken message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: messaging.IUserToken, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Decodes a UserToken message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns UserToken
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): messaging.UserToken;

        /**
         * Decodes a UserToken message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns UserToken
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): messaging.UserToken;

        /**
         * Verifies a UserToken message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);

        /**
         * Creates a UserToken message from a plain object. Also converts values to their respective internal types.
         * @param object Plain object
         * @returns UserToken
         */
        public static fromObject(object: { [k: string]: any }): messaging.UserToken;

        /**
         * Creates a plain object from a UserToken message. Also converts values to other types if specified.
         * @param message UserToken
         * @param [options] Conversion options
         * @returns Plain object
         */
        public static toObject(message: messaging.UserToken, options?: $protobuf.IConversionOptions): { [k: string]: any };

        /**
         * Converts this UserToken to JSON.
         * @returns JSON object
         */
        public toJSON(): { [k: string]: any };
    }

    /** Properties of a Container. */
    interface IContainer {

        /** Container userID */
        userID?: (string|null);

        /** Container gameID */
        gameID?: (string|null);

        /** Container gatewayID */
        gatewayID?: (string|null);

        /** Container connection */
        connection?: (messaging.IConnection|null);

        /** Container gameStatusUpdate */
        gameStatusUpdate?: (messaging.IGameStatusUpdate|null);

        /** Container playerUpdate */
        playerUpdate?: (messaging.IPlayerUpdate|null);

        /** Container hostUpdate */
        hostUpdate?: (messaging.IHostUpdate|null);

        /** Container userToken */
        userToken?: (messaging.IUserToken|null);

        /** Container openGame */
        openGame?: (messaging.IOpenGame|null);

        /** Container kickUser */
        kickUser?: (messaging.IKickUser|null);
    }

    /** Represents a Container. */
    class Container implements IContainer {

        /**
         * Constructs a new Container.
         * @param [properties] Properties to set
         */
        constructor(properties?: messaging.IContainer);

        /** Container userID. */
        public userID: string;

        /** Container gameID. */
        public gameID: string;

        /** Container gatewayID. */
        public gatewayID: string;

        /** Container connection. */
        public connection?: (messaging.IConnection|null);

        /** Container gameStatusUpdate. */
        public gameStatusUpdate?: (messaging.IGameStatusUpdate|null);

        /** Container playerUpdate. */
        public playerUpdate?: (messaging.IPlayerUpdate|null);

        /** Container hostUpdate. */
        public hostUpdate?: (messaging.IHostUpdate|null);

        /** Container userToken. */
        public userToken?: (messaging.IUserToken|null);

        /** Container openGame. */
        public openGame?: (messaging.IOpenGame|null);

        /** Container kickUser. */
        public kickUser?: (messaging.IKickUser|null);

        /** Container msg. */
        public msg?: ("connection"|"gameStatusUpdate"|"playerUpdate"|"hostUpdate"|"userToken"|"openGame"|"kickUser");

        /**
         * Creates a new Container instance using the specified properties.
         * @param [properties] Properties to set
         * @returns Container instance
         */
        public static create(properties?: messaging.IContainer): messaging.Container;

        /**
         * Encodes the specified Container message. Does not implicitly {@link messaging.Container.verify|verify} messages.
         * @param message Container message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: messaging.IContainer, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Encodes the specified Container message, length delimited. Does not implicitly {@link messaging.Container.verify|verify} messages.
         * @param message Container message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: messaging.IContainer, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Decodes a Container message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns Container
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): messaging.Container;

        /**
         * Decodes a Container message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns Container
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): messaging.Container;

        /**
         * Verifies a Container message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);

        /**
         * Creates a Container message from a plain object. Also converts values to their respective internal types.
         * @param object Plain object
         * @returns Container
         */
        public static fromObject(object: { [k: string]: any }): messaging.Container;

        /**
         * Creates a plain object from a Container message. Also converts values to other types if specified.
         * @param message Container
         * @param [options] Conversion options
         * @returns Plain object
         */
        public static toObject(message: messaging.Container, options?: $protobuf.IConversionOptions): { [k: string]: any };

        /**
         * Converts this Container to JSON.
         * @returns JSON object
         */
        public toJSON(): { [k: string]: any };
    }

    /** CardDeckType enum. */
    enum CardDeckType {
        DEFAULT = 0,
        TSHIRT = 1
    }

    /** CardDeckCards enum. */
    enum CardDeckCards {
        SPECIAL_NOTCHOSEN = 0,
        SPECIAL_CHOSEN = 1,
        SPECIAL_JOINMAJORITY = 2,
        SPECIAL_NOMAJORITY = 3,
        DEFAULT_ZERO = 4,
        DEFAULT_PFIVE = 5,
        DEFAULT_ONE = 6,
        DEFAULT_TWO = 7,
        DEFAULT_THREE = 8,
        DEFAULT_FIVE = 9,
        DEFAULT_EIGHT = 10,
        DEFAULT_THIRTEEN = 11,
        DEFAULT_TWENTY = 12,
        DEFAULT_FOURTY = 13,
        DEFAULT_HUNDRED = 14,
        TSHIRT_S = 15,
        TSHIRT_M = 16,
        TSHIRT_L = 17,
        TSHIRT_XL = 18
    }
}
