import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import {WebsocketPlugin} from './websocket/wss'
import './assets/styles/index.css';
import 'vue-scrollto'

const VueScrollTo = require('vue-scrollto');
Vue.use(VueScrollTo)

Vue.config.productionTip = false
Vue.use(WebsocketPlugin , {
  store
})

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
