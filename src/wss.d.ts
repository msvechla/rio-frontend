import Vue from 'vue'
import {messaging} from "./../rio-proto.js";

declare module 'vue/types/vue' {
	interface Vue {
		$websocketConnect: (
			gameID: string,
			hosting: boolean,
			playerName?: string,
		) => void,
			
		$websocketSend: (
			c: messaging.Container,
		) => void
	}
}
